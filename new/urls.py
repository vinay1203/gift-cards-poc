from django.conf.urls import url
from django.conf.urls.static import static
from django.views.generic import TemplateView

from gift_card_proj import settings
from new import views
from django.urls import path, re_path, include

# from wagtail.admin import urls as wagtailadmin_urls
# from wagtail.documents import urls as wagtaildocs_urls
# from wagtail.core import urls as wagtail_urls


urlpatterns = [
    url(r'login', views.user_login, name='login'),
    url(r'products', views.products, name='products'),
    url(r'success', TemplateView.as_view(template_name='success.html'), name='success'),
    url(r'add_product/(?P<p_id>\w+)', views.add_product, name='add_product'),
    # re_path(r'^cms/', include(wagtailadmin_urls)),
    # re_path(r'^documents/', include(wagtaildocs_urls)),
    # re_path(r'^pages/', include(wagtail_urls)),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)