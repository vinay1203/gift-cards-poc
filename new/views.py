import json
import math
import random

import requests
from django.contrib import auth
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse

from new.models import Product


def user_login(request):
    if request.method == 'GET':
        return render(request, 'login.html')

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username=username, password=password)

        if user is not None:
            # correct username and password login the user
            auth.login(request, user)
            return redirect('products')

        else:
            return redirect('login')

    return render(request, 'login.html')


def products(request):
    if request.method == 'GET':
        products = Product.objects.all()
        return render(request, 'products.html', {'products': products})


def generateOTP():
    # Declare a digits variable
    # which stores all digits
    digits = "0123456789"
    otp = ""

    # length of password can be chaged
    # by changing value in range
    for i in range(4):
        otp += digits[math.floor(random.random() * 10)]

    return otp


def add_product(request, p_id):
    if request.method == 'GET':
        try:
            product = Product.objects.filter(id=p_id)
            url = "https://catalog.vouchagram.net/EPService.svc/PullVoucher?BuyerGuid=3627544B-ED62-4483-A6EE-8C7BBA987308&ProductGuid={}&ExternalOrderID={}&Quantity={}&Password=7757FE887C5".format(product[0].product_id, generateOTP(), 1,)
            response = requests.get(url=url)
            data = json.loads(response.text)
            voucher_data = data['vPullVouchersResult']['PullVouchers'][0]['Vouchers']
            product_name = data['vPullVouchersResult']['PullVouchers'][0]['ProductName']
            end_date = voucher_data[0]['EndDate']
            value = voucher_data[0]['Value']
            voucher_code = voucher_data[0]['VoucherGCcode']
            voucher_no = voucher_data[0]['VoucherNo']
            voucher_pin = voucher_data[0]['Voucherpin']
            print("Hii {}, Thanks for ordering your voucher code for {} is {}, voucher pin {}, expiry {}, voucher value {},  voucher no {}".format("vinay", product_name, voucher_code, voucher_pin, end_date, value, voucher_no))
            send_sms(end_date, value, voucher_code, product_name, voucher_no, voucher_pin)
            return redirect('success')
        except:
            return render(request, 'products.html', {'message': "Product not available"})


def send_sms(end_date, value, voucher_code, product_name, voucher_no, voucher_pin):

    url = "https://api-alerts.kaleyra.com/v4/"

    querystring = {"method": "sms", "sender": "DSENSE", "to": "91" + str(8125895987),
                   "message": "Hii {}, Thanks for ordering your voucher code is {}, voucher pin {}, expiry {}, voucher value {},  voucher no {}".format("vinay", voucher_code, voucher_pin, end_date, value, voucher_no),
                   "api_key": "Ae273b4bb5cfbf484ac1df1b8c3b5fdc3"}
    response = requests.request("GET", url, params=querystring)
    return response


# import pysnooper
#
#
# @pysnooper.snoop()
# def number_to_bits(number):
#     if number:
#         bits = []
#         while number:
#             number, remainder = divmod(number, 2)
#             bits.insert(0, remainder)
#         return bits
#     else:
#         return [0]
#
#
# number_to_bits(6)

