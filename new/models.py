from django.db import models


class Product(models.Model):
    product_name = models.CharField(max_length=200, null=True, blank=True)
    product_id = models.CharField(max_length=200, null=True, blank=True)
    product_price = models.FloatField(null=True, default=0, blank=True)
    product_images = models.ImageField(upload_to='static/images/products', null=True, blank=True)

    def __str__(self):
        return self.product_name

    class Meta:
        verbose_name_plural = 'Product'


class Person(models.Model):
    name = models.CharField(max_length=50)


class Group(models.Model):
    name = models.CharField(max_length=128)
    members = models.ManyToManyField(
        Person,
    )


class Membership(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    inviter = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        related_name="membership_invites",
    )
    invite_reason = models.CharField(max_length=64)
