from django.contrib import admin

from new.models import Product, Person, Group, Membership

admin.site.register(Product)
admin.site.register(Person)
admin.site.register(Group)
admin.site.register(Membership)